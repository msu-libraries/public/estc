from pprint import pprint
from pymarc import MARCReader
from urlparse import urlparse, parse_qs
from formatobject.formatobject import FormatObject
import os
import json

class EstcMetadata():

    def __init__(self, metadata_dir):
        self.metadata_dir = metadata_dir
        self.marc_files = (f for f in os.listdir(metadata_dir) if f.endswith(".mrc"))
        self.field = None
        self.results = {}
        self.previous_nums = ['N010482',
                             'N001066',
                             'N012543',
                             'N013180',
                             'N013322',
                             'N014840',
                             'N017277',
                             'N017370',
                             'N017898',
                             'N018908',
                             'N024599',
                             'N024679',
                             'N024754',
                             'N025134',
                             'N025400',
                             'N027238',
                             'N027579',
                             'N002924',
                             'N002926',
                             'N002927',
                             'N031070',
                             'N003516',
                             'N003730',
                             'N052347']
        
    def set_subjects(self, subject_list="Default"):


        self.field = "subject"
        default_subjects = ["radicals", "jacobins", "jacobites", "socialism", "socialism and liberty", "socialist property"]

        if os.path.exists(subject_list):
            with open(subject_list, "r") as f:
                self.subject_list = json.load(f)

        elif subject_list == "Default" or not isinstance(list, subject_list):
            print "Using default subject list."
            self.subject_list = default_subjects

        else:
            self.subject_list = subject_list


    def __print_count(self):
        
        if self.__i%1000 == 0:
            
            sys.stdout.write("Processed {0} Records".format(self.__i))
            sys.stdout.flush()


    def __clean_string(self, string):

        return string.strip().lower().rstrip(";./,:")  


    def get_matches(self):

        self.estc_nums = []

        if self.field is None:
            print "First set match parameters using e.g. 'set_subjects.'"
            return

        self.__i = 0
        for mfile in self.marc_files:
            mfile_path = os.path.join(self.metadata_dir, mfile)
            self._open_reader(mfile_path)

        print "Processed {0} Records".format(self.__i) 
        for result in self.results:
            print result, self.results[result]
        print "Retrieved {0} ESTC Numbers".format(len(self.estc_nums))

    def write_matches_to_file(self, path):
        with open(path, "w") as f:
            for num in self.estc_nums:
                f.write(num+"\n")
        print "Wrote {0} ESTC numbers".format(len(self.estc_nums))

    def _open_reader(self, file_path):

        with open(file_path, "rb") as f:
            reader = MARCReader(f)
            for record in reader:  
                self.record = record      
                self.__i += 1
                self.__print_count()
                self._test_record(self.record)


    def _test_record(self, record):

        if self.field == "subject":
            for subject in record.subjects():
                if subject is not None and subject["a"] is not None:
                    subject_text = self.__clean_string(subject["a"])
                    if subject_text in self.subject_list:
                        self._return_result(subject_text)

    @staticmethod
    def get_subject_list(file_path):

        new_sub_list = FormatObject()
        new_sub_list.write_json_from_file(file_path)


    def _return_result(self, value):

        self.results[value] = self.results.get(value, 0) + 1
        #print "FOUND VALUE", value, "IN RECORD"
        #print "Checking 856..."
        self.__check_ecco()


    def __check_ecco(self):

        for loc in self.record.get_fields("856"):
            #print loc["y"]
            if loc["y"] and u'Eighteenth Century' in loc["y"]:

                self.__get_estc_num(loc)

            else:

                pass

    def __get_estc_num(self, loc):

        if loc["u"] and "http" in loc["u"]:
            estc_num = self.__parse_url(loc["u"])[0]
            if estc_num not in self.previous_nums:
                self.estc_nums.append(estc_num)

        else:

            pass

    def __parse_url(self, url):

        query = urlparse(url).query
        query_dict = parse_qs(query)
        return query_dict["ae"]


"""

                    for subject in record.get_fields("650"):
                        if subject is not None and subject["a"] is not None:
                            subject_650a = subject["a"].strip().lower().rstrip(";./,;:").decode("utf8")
                            if subject_650a not in all_subjects:
                                all_subjects.append(subject_650a)
                            if "radicalism" in subject_650a:
                                print record




all_subjects = []
mdir = "/Users/higgi135/Data/metadata4siro/estcMeta"
marc_files = [f for f in os.listdir(mdir) if f.endswith(".mrc")]
i=0
for mfile in marc_files:
    mpath = os.path.join(mdir, mfile)
    with open(mpath, "rb") as fh:
        reader = MARCReader(fh)


print "Processed {0} Records".format(i)
subjects_file = os.path.join(mdir, "all_subjects.csv")
with open(subjects_file, "w") as f:

    for sub in sorted(all_subjects):
        f.write(sub.encode("utf8")+",\n")

pprint(len(all_subjects))


                
"""
